# playstat

_(Default chars per second: 11.36)_

## Usage

Todo.

## Stageplays

The conventions for the stageplay markdown:

- We use [Pandoc flavored markdown](https://pandoc.org/MANUAL.html#pandocs-markdown)
- Dialogs are definition list
- A tab is represented as four spaces
- For breaking inside verses just use a single break
- If there is more than one speaker, seperate them by comma and a space (`, `)
- Terms like «All», «Alle», «Both» and «Beide» are ignored.

Example md file:

```markdown
# Act I
## Scene I.1 − Some Faust

_Margarete springt herein._

Margarete
:   Er kommt!

Faust
:   _(Kommt.)_ Ach, Schelm, so neckst du mich!  
Treff ich dich! _(Er küßt sie.)_

Margarete
:   _(Ihn fassend und den Kuß zurückgebend.)_  
Bester Mann! von Herzen lieb ich dich!

_Mephistopheles klopft an._

Faust, Margarete
:   _(Stampfend.)_ Wer da?

Mephistopheles
:   Gut Freund!
```
