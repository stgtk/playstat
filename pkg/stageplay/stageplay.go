package stageplay

import (
	"gitlab.com/solutionsbuero/gosmooth"
	"io/ioutil"
	"log"
	"os"
)

type Stageplay struct {
	Path    string
	Content string
}

func NewStageplay(path string) Stageplay {
	smooth := gosmooth.NewSmooth(path, "")
	smooth.DoM4()

	file, err := os.Open(smooth.Document.Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	return Stageplay{
		Path:    smooth.Document.Path,
		Content: string(b),
	}
}
