package statistics

import (
	"fmt"
	"gitlab.com/stgtk/playstat/pkg/stageplay"
	"regexp"
	"strings"
	"time"
)

const defaultCharsPerSecond = 11.36

type Statistics struct {
	Stageplay      stageplay.Stageplay
	Actors         map[string]int
	PlayLength     int
	PlayDuration   time.Duration
	CharsPerSecond float64
}

func NewStatistics(stageplay stageplay.Stageplay) Statistics {
	stats := Statistics{
		Stageplay:      stageplay,
		CharsPerSecond: defaultCharsPerSecond,
	}

	stats.analyzePlayLength()
	stats.calculateRunningTime()
	// stats.analyzeActors()
	return stats
}

// LENGTH

func (s *Statistics) analyzePlayLength() {
	re := regexp.MustCompile(`[: ] {3}(.*)\n`)
	for _, result := range re.FindAllStringSubmatch(s.Stageplay.Content, -1) {
		s.PlayLength = s.PlayLength + len(strings.TrimSpace(result[1]))
	}
}

func (s *Statistics) calculateRunningTime() {
	if s.PlayLength == 0 {
		return
	}

	s.PlayDuration = time.Duration(float64(s.PlayLength) / s.CharsPerSecond) * time.Second

}

// ACTORS

func (s *Statistics) analyzeActors() {
	re := regexp.MustCompile(`(.*)\n: {3}(.*)\n( {4}(.*)\n)*`)
	matches := re.FindAllStringSubmatch(s.Stageplay.Content, -1)

	var actors []string
	for _, match := range matches {
		handleActorNames(match[1], &actors)
	}
	fmt.Println(actors)

}

func handleActorNames(raw string, actors *[]string) {
	raw = strings.TrimPrefix(raw, "(")

	newActors := strings.Split(raw, ", ")
	for _, newActor := range newActors {
		if !contains(*actors, newActor) {
			*actors = append(*actors, newActor)
		}
	}
}

func contains(list []string, item string) bool {
	for i, _ := range list {
		if list[i] == item {
			return true
		}
	}
	return false
}
