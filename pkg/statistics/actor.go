package statistics

type Actor struct {
	Name        string
	SpeakLength int
}
