package main

import (
        "errors"
	"fmt"
	"github.com/urfave/cli"
	"gitlab.com/stgtk/playstat/pkg/stageplay"
	"gitlab.com/stgtk/playstat/pkg/statistics"
	"log"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "playstat"
	app.Description = "Statistics for your markdown stageplays"
	app.Action = MainAction

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func MainAction(c *cli.Context) error {
	if len(c.Args()) == 0 {
		return errors.New("No input file provided")
	}
	stats := statistics.NewStatistics(stageplay.NewStageplay(c.Args()[0]))

	fmt.Printf("Length: %d chars\n", stats.PlayLength)
	fmt.Printf("Estimated duration: %s\n", stats.PlayDuration)
	return nil
}
